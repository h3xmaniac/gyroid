#!/bin/bash
git pull
source ~/.profile
pyenv local 3.6.5
python3 -m pip install --upgrade pip
python3 -m pip install  --upgrade -r requirements.txt
python3 gyroid.py
while [ -f restart.txt ]
do
    git pull
    python3 gyroid.py
done