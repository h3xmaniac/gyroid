import discord
from discord.ext import commands
from fuzzywuzzy import process
from lib import db
from lib import uni
import config


class VillagerInfo():
    
    
    def __init__(self, bot):
        self.bot = bot
        with open("assets/villagerdata.txt","r",encoding="utf-8") as file:
            self.vinfo = [[j.strip() for j in i.split("|")] for i in file.readlines()]
            
        
    
    
    @commands.command(pass_context=True,aliases=["vinfo","villager"])
    async def villagerinfo(self, ctx, *, villager):
        villagerlist = [await uni.translate(ctx,x[0]) for x in self.vinfo]
        
        match = process.extractOne(villager,villagerlist)
        
        if match[1] < 75:
            return await ctx.send((await uni.translate(ctx,"common.nan")).format("villager",match[0],str(match[1])))
        embed = discord.Embed(name=match[0],description=(await uni.translate(ctx,"common.closematch")).format(match[0]))
        
        data = [x for x in self.vinfo if await uni.translate(ctx,x[0]) == match[0]]
        thing = ["villagers.name","villagers.gender","personalities.personality","species.species","months.birthday","villagers.catchphrase"]
        translated = [await uni.translate(ctx,x) for x in thing]
        
        for x in range(0,len(thing)):
            if x == 4:
                translate = await ctx.translate(ctx,(data[4].split(" ")[0])) + data[4].split(" ")[1]
            else:
                try:
                    translate = await ctx.translate(ctx, data[x])
                except:
                    translate = data[x]
            embed.add_field(name=translated[x],value=translate)
        await ctx.send(embed=embed)
    
def setup(bot):
    pass#bot.add_cog(VillagerInfo(bot))
        
        
 
        
        
        