from assets.musiclist import *
musiclist = []
for i in music:
    musiclist += music[i]
namelist = []
for i in trackNames:
    namelist += trackNames[i]
mus = {namelist[i]:musiclist[i] for i in range(len(musiclist))}
import discord, random
from discord.ext import commands
from lib import uni
from fuzzywuzzy import process
import os
import asyncio


class Song():

    def __init__(self, file, trackname, requested_by, playkey, q):
        self.file = file
        self.trackname = trackname
        self.requested_by = requested_by
        self.playkey = playkey
        self.q = q

def audio_playing(ctx):
    """Checks that audio is currently playing before continuing."""
    client = ctx.guild.voice_client
    if client and client.channel and client.source:
        return True
    return False

def in_voice_channel(ctx):
    """Checks that the command sender is in the same voice channel as the bot."""
    voice = ctx.author.voice
    bot_voice = ctx.guild.voice_client
    if voice and bot_voice and voice.channel and bot_voice.channel and voice.channel == bot_voice.channel:
        return True
    return False


def is_audio_requester(ctx):
    music = ctx.bot.get_cog("Music")
    state = music.get_state(ctx.guild)
    permissions = ctx.channel.permissions_for(ctx.author)
    if permissions.administrator or state.is_requester(ctx.author) or ctx.author.id in [232948417087668235,124316478978785283,131131701148647424]:
        return True
    return False



class GuildState():

    def __init__(self):
        self.volume = 1.0
        self.playlist = []
        #self.skip_votes = set()
        self.now_playing = None
    
    def is_requester(self, user):
        return self.now_playing.requested_by == user


class Colours:
    queue = discord.Colour(0x7988ff)
    play = discord.Colour(0x790079)
    np = discord.Colour(0xff88ff)
class Music:
    def __init__(self,bot):
        self.bot = bot
        #self.playingin = {}
        self.states = {}
        self.devmailchannel = bot.get_channel(503688326079971341)
        # discord.opus.load_opus()
    async def send_embed(self,ctx,song,optype=0):
        track = song.trackname
        requested_by = song.requested_by
        key = song.playkey
        q = song.q
        colour = Colours.play
        title = await uni.translate(ctx,'music.misc.play')
        if optype == 1:
            colour = Colours.queue
            title = await uni.translate(ctx,'music.misc.queue')
            key = q
        elif optype == 2:
            colour = Colours.np
            title = await uni.translate(ctx,'music.misc.np')
            key = await uni.translate(ctx,'music.np.np')
        # embed = discord.Embed(title=title,colour=colour,description=(await uni.translate(ctx,key)))
        embed = discord.Embed(title=title,colour=colour,description=(await uni.translate(ctx,key)).format(await uni.translate(ctx,track)))
        tmp = 'music.misc.incorrect'
        if (await uni.translate(ctx,track,'en')).startswith('Unknown'): tmp = 'music.misc.unknown'
        embed.set_footer(text=(await uni.translate(ctx,tmp)).format(track).replace('[p]',self.bot.command_prefix(self.bot,ctx)[0]) + ' | ' + (await uni.translate(ctx,'music.misc.requested_by')).format(requested_by))
        await ctx.send(embed=embed)


    async def play_song(self, ctx,client, state, song):
        await self.send_embed(ctx,song)
        file = song.file
        state.now_playing = song 
        audio = discord.PCMVolumeTransformer(discord.PCMAudio(open('assets/snd/'+file,'rb')),volume=state.volume)

        async def async_after():
            if len(state.playlist) > 0:
                song = state.playlist.pop(0)
                ctx.author = song.requested_by
                await self.play_song(ctx, client,state,song)
            else:
                await ctx.send(await uni.translate(ctx,'music.play.end'))
                await client.disconnect()
                del self.states[client.guild.id]

        def after(err):
            print(err)
            if not err: self.bot.loop.create_task(async_after()) 
            
        client.play(audio,after=after)







    async def get_music(self, ctx, category_or_track):
        if category_or_track:
            try:
                options = music[category_or_track.lower()]
                trackno = random.randint(0,len(options)-1)
                file = options[trackno]+'.wav'
                track = trackNames[category_or_track.lower()][trackno]
                queuename = (await uni.translate(ctx,"music.play.queuecategory")).format(category_or_track.title(),'{}')
                playingname = (await uni.translate(ctx,'music.play.playingcategory')).format(category_or_track.title(),'{}')
            except:
                if category_or_track.upper()+'.wav' in os.listdir('./assets/snd'):
                    file = category_or_track.upper()+'.wav'
                    track = category_or_track.upper()
                    queuename = 'music.play.queuebyfilename'
                    playingname = 'music.play.playingbyfilename'
                else:
                    translated = {(await uni.translate(ctx,i)).lower():i for i in namelist}
                    file = process.extractOne(category_or_track.lower(),translated.keys())[0]
                    print(translated,file)
                    track = translated[file]
                    file = mus[track]+'.wav'
                    print(track,file)
                    queuename = 'music.play.queue'
                    playingname = 'music.play.playing'
        else:
            track = random.choice(list(mus.keys()))
            file = mus[track]+'.wav'
            queuename = 'music.play.queuerandom'
            playingname = 'music.play.playingrandom'
        return Song(file,track,ctx.author, playingname, queuename)






    def get_state(self, guild):
        if guild.id not in self.states:
            self.states[guild.id] = GuildState()
        return self.states[guild.id]





    async def checkthree(self, ctx):
        if audio_playing(ctx):
            if in_voice_channel(ctx):
                if is_audio_requester(ctx):
                    return True
                else:
                    await ctx.send(await uni.translate(ctx,"music.misc.notrequester"))
            else:
                await ctx.send(await uni.translate(ctx, "music.misc.notinvc"))
        else:
            await ctx.send(await uni.translate(ctx, "music.misc.noaudio"))
        return False

    @commands.command()
    async def pause(self, ctx):
        if await self.checkthree(ctx):
            client = ctx.guild.voice_client
            if client.is_paused():
                client.resume()
            else:
                client.pause()
    
    @commands.command()
    async def volume(self, ctx, volume):
        if await self.checkthree(ctx):
            volume = int(volume.strip("%"))
            volume = 0 if volume < 0 else 100 if volume > 100 else volume
            client = ctx.guild.voice_client
            state = self.get_state(ctx.guild)
            state.volume =  float(volume/100)
            client.source.volume = state.volume
                
    @commands.command(aliases=['s'])
    async def skip(self, ctx):
        if await self.checkthree(ctx):
            client = ctx.guild.voice_client
            client.stop()        
    
    @commands.command(aliases=["np"])
    async def nowplaying(self, ctx):
        if audio_playing(ctx):
            state = self.get_state(ctx.guild)
            # await ctx.send(state.now_playing.trackname)
            await self.send_embed(ctx,state.now_playing,2)
        else:
            await ctx.send(await uni.translate(ctx, "music.misc.noaudio"))

    @commands.command(aliases=["disconnect"])
    async def stop(self,ctx):
        client = ctx.guild.voice_client
        state = self.get_state(ctx.guild)
        if client and client.channel:
            state.playlist = []
            await client.disconnect()

    @commands.command(help='music.play.help')
    async def play(self, ctx, *, category_or_track=None):
        if ctx.guild.voice_client and ctx.guild.voice_client.is_paused() and not category_or_track:
            ctx.guild.voice_client.resume()
            return
        voice = ctx.author.voice
        if voice:
            if voice.deaf or voice.self_deaf:
                await ctx.send(await uni.translate(ctx, "music.play.deafened"))
                return
            client = ctx.guild.voice_client
            state = self.get_state(ctx.guild)
            song = await self.get_music(ctx, category_or_track)
            if client and client.channel:
                if client.channel != voice.channel:
                    await ctx.send(await uni.translate(ctx,'music.play.wrongchannel'))
                state.playlist.append(song)
                await self.send_embed(ctx,song,1)
            else:
                client = await voice.channel.connect()
                # await self.send_embed(ctx,song,1)
                await self.play_song(ctx,client,state,song)
        else:
            await ctx.send(await uni.translate(ctx,"music.play.notconnected"))


    """
    @commands.command(help='music.play.help',pass_context=True)
    async def play(self,ctx,*,category_or_track=None):
        print(ctx)
        voice = ctx.author.voice
        if voice:
            if voice.deaf or voice.self_deaf:
                await ctx.send(await uni.translate(ctx,'music.play.deafened'))
            else:
                if self.playingin.get(ctx.guild.id,None):
                    await ctx.send(await uni.translate(ctx,'music.play.alreadyplaying'))
                else:
                    file, playingname = await self.get_music(category_or_track)
                    channel = voice.channel
                    if channel.guild.voice_client:
                        await channel.guild.voice_client.disconnect()
                    self.playingin[ctx.guild.id] = await channel.connect()
                    audio = discord.PCMAudio(open('assets/snd/'+file,'rb'))
                    async def docallback(error=None):
                        print("DONE")   
                        await self.playingin[ctx.guild.id].disconnect()
                        print("Should have disconnected")
                        del self.playingin[ctx.guild.id] 
                    def callback(error=None):
                        try:
                            self.bot.loop.create_task(docallback(error))       
                        except Exception as e:
                            print(e)
                    self.playingin[ctx.guild.id].play(audio,after=callback)
                    await ctx.send(playingname)
        else:
            await ctx.send(await uni.translate(ctx,'music.play.notconnected'))
    """
    @commands.command(help='music.devmail.help')
    async def devmail(self,ctx,*,message):
        await self.devmailchannel.send(f'{ctx.author} ({ctx.author.id}): {message}')
        await ctx.send(await uni.translate(ctx,'music.devmail.sent'))
def setup(bot):
    bot.add_cog(Music(bot))