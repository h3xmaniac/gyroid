import discord
from discord.ext import commands
import aiosqlite
from lib import db
from lib import uni
import re
#t = uni.translate

class Profile:
    def __init__(self, bot):
        self.bot = bot
        self.db = db
        with open('assets/villagerdata.txt') as file:
            self.villagerdata = [i.split(' | ') for i in file.read().split('\n')]
        self.villagernames = [i[0] for i in self.villagerdata]
    
    @commands.group(help='profile.profile.help',invoke_without_command=True)
    async def profile(self,ctx, target:discord.User=None):
        if ctx.invoked_subcommand is None:
            if not target:
                target = ctx.author
            prof = await db.getprof(target.id,ctx)
            if prof == {'locale':'en'}:
                await ctx.send(await uni.translate(ctx,'profile.{}noprofile'.format('youhave' if ctx.author == target else 'theyhave')))
                return
            embed = discord.Embed(title=(await uni.translate(ctx,'profile.profile.embed.title')).format(target))
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.3ds'),value=prof['3ds'],inline=True)
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.hacker'),value=(await uni.translate(ctx,'common.'+('mhm' if prof['hack'] else 'smh')) if prof['hack'] != await uni.translate(ctx,'common.unset') else prof['hack']),inline=True)
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.fc'),value=prof['fc'])
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.ign'),value=prof['ign'],inline=True)
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.town'),value=prof['town_n'],inline=True)
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.villager'),value=await uni.translate(ctx,prof['fav_vil']))
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.fruit'),value=await uni.translate(ctx,prof['fruit']),inline=True)
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.pwp'),value=prof['pub_works'],inline=True)
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.dream'),value=prof['dream_add'])
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.furniture'),value=prof['furniture_sets'],inline=True)
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.badges'),value=prof['badges'],inline=True)
            embed.add_field(name=await uni.translate(ctx,'profile.profile.embed.songs'),value=prof['slider'])
            embed.set_footer(text=(await uni.translate(ctx,'profile.profile.embed.requestedby')).format(ctx.author))
            await ctx.send(embed=embed)
    @profile.command(help='profile.profile.ign.help')
    async def ign(self,ctx,*,ign):
        if len(ign) < 10:
            await db.insert(ctx.author.id,'ign',ign)
            await ctx.send(await uni.translate(ctx,'profile.profile.ign.set'))
        else:
            await ctx.send(await uni.translate(ctx,'profile.profile.ign.long'))
    @profile.command(help='profile.profile.town.help')
    async def town(self,ctx,*,townname):
        if len(townname) < 10:
            await db.insert(ctx.author.id,'town_n',townname)
            await ctx.send(await uni.translate(ctx,'profile.profile.town.set'))
        else:
            await ctx.send(await uni.translate(ctx,'profile.profile.town.long'))
    @profile.command(help='profile.profile.3ds.help',name='3ds')
    async def _3ds(self,ctx,*,fc):
        if re.match(r'^(\d{4} ?\-? ?){3}$',fc):
            fc = fc.replace(' ','').replace('-','')
            fc = fc[:4]+' - '+fc[4:8]+' - '+fc[8:]
            await db.insert(ctx.author.id,'3ds',fc)
            await ctx.send(await uni.translate(ctx,'profile.profile.3ds.set'))
        else:
            await ctx.send(await uni.translate(ctx,'profile.profile.3ds.invalid'))
    @profile.command(help='profile.profile.dream.help')
    async def dream(self,ctx,*,address):
        if re.match(r'^([0-9a-fA-F]{4} ?\-? ?){3}$',address):
            address = address.replace(' ','').replace('-','')
            address = address[:4]+' - '+address[4:8]+' - '+address[8:]
            await db.insert(ctx.author.id,'dream_add',address)
            await ctx.send(await uni.translate(ctx,'profile.profile.dream.set'))
        else:
            await ctx.send(await uni.translate(ctx,'profile.profile.dream.invalid'))
    @profile.command(help='profile.profile.fc.help')
    async def fc(self,ctx,*,fc):
        if re.match(r'^(SW)? ?\-? ?(\d{4} ?\-? ?){3}$',fc):
            fc = fc.replace(' ','').replace('-','').lstrip('SW -')
            fc = fc[:4]+' - '+fc[4:8]+' - '+fc[8:]
            await db.insert(ctx.author.id,'fc','SW - '+fc)
            await ctx.send(await uni.translate(ctx,'profile.profile.fc.set'))
        else:
            await ctx.send(await uni.translate(ctx,'profile.profile.fc.invalid'))
    @profile.command(help='profile.profile.fruit.help')
    async def fruit(self,ctx,fruitname):
        validfruit = ['profile.profile.fruit.fruit.apples','profile.profile.fruit.fruit.cherries','profile.profile.fruit.fruit.peaches','profile.profile.fruit.fruit.pears','profile.profile.fruit.fruit.oranges']
        found = False
        for i in validfruit:
            if (await uni.translate(ctx,i)).casefold() == fruitname.casefold():
                found = i
                break
        if found:
            await db.insert(ctx.author.id,'fruit',found)
            await ctx.send(await uni.translate(ctx,'profile.profile.fruit.set'))
        else:
            await ctx.send(await uni.translate(ctx,'profile.profile.fruit.invalid'))
    @profile.command(help='profile.profile.hacker.help')
    async def hacker(self,ctx,hacker:bool):
        await db.insert(ctx.author.id,'hack',int(hacker))
        await ctx.send((await uni.translate(ctx,'profile.profile.hacker.set')).format((await uni.translate(ctx,'common.mhm')).lower() if hacker else (await uni.translate(ctx,'common.smh')).lower()))
    @profile.command(help='profile.profile.furniture.help')
    async def furniture(self,ctx,sets:int):
        #37 sets
        if sets > 37:
            await ctx.send(await uni.translate(ctx,'profile.profile.furniture.many'))
        else:
            await db.insert(ctx.author.id,'furniture_sets',sets)
            await ctx.send((await uni.translate(ctx,'profile.profile.furniture.set')).format(sets))
    @profile.command(help='profile.profile.songs.help')
    async def songs(self,ctx,songs:int):
        #91 songs
        if songs > 91:
            await ctx.send(await uni.translate(ctx,'profile.profile.songs.many'))
        else:
            await db.insert(ctx.author.id,'slider',songs)
            await ctx.send((await uni.translate(ctx,'profile.profile.songs.set')).format(songs))
    @profile.command(help='profile.profile.badges.help')
    async def badges(self,ctx,badges:int):
        #72 badges
        if badges > 72:
            await ctx.send(await uni.translate(ctx,'profile.profile.badges.many'))
        else:
            await db.insert(ctx.author.id,'badges',badges)
            await ctx.send((await uni.translate(ctx,'profile.profile.badges.set')).format(badges))
    @profile.command(help='profile.profile.villager.help')
    async def villager(self,ctx,villager):
        found = False
        for i in self.villagernames:
            if villager.casefold() == (await uni.translate(ctx,i)).casefold():
                found = i
                break
        if found:
            await self.db.insert(ctx.author.id,'fav_vil',found)
            await ctx.send(await uni.translate(ctx,'profile.profile.villager.set'))
        else:
            await ctx.send(await uni.translate(ctx,'profile.profile.villager.invalid'))
    @profile.command(help='profile.profile.pwp.help')
    async def pwps(self,ctx,pwps:int):
        #30 pwps max
        if pwps > 30:
            await ctx.send(await uni.translate(ctx,'profile.profile.pwp.many'))
        else:
            await self.db.insert(ctx.author.id,'pub_works',pwps)
            await ctx.send((await uni.translate(ctx,'profile.profile.pwp.set')).format(pwps))
    @profile.command(help='profile.profile.locale.help')
    async def locale(self,ctx,locale=None):
        if locale == None:
            await ctx.send((await uni.translate(ctx,'profile.profile.locale.current')).format((await self.db.getprof(ctx.author.id,ctx))['locale']))
        else:
            await self.db.insert(ctx.author.id,'locale',locale.casefold())
            await uni.clear_cache(ctx)
            await ctx.send(await uni.translate(ctx,'profile.profile.locale.set'))
def setup(bot):
    bot.add_cog(Profile(bot))