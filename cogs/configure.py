import discord, aiofiles
from discord.ext import commands
from lib import uni
class Configuration:
    def __init__(self,bot):
        self.bot = bot
    #https://discord.gg/FYCVeC
    @commands.command(help='configuration.setprefix.help')
    @commands.has_permissions(manage_guild=True)
    async def setprefix(self,ctx,newprefix):
        newprefix = newprefix.lstrip(' ')
        if len(newprefix) > 10:
            await ctx.send(await uni.translate(ctx,'configuration.setprefix.long'))
            return
        add = (await uni.translate(ctx,'configuration.setprefix.removed') if newprefix == '' else (await uni.translate(ctx,'configuration.setprefix.changed')).format(newprefix)) if ctx.guild.id in self.bot.additionalprefixdata else (await uni.translate(ctx,'configuration.setprefix.set')).format(newprefix)
        outmsg = (await uni.translate(ctx,'congiguration.setprefix.formatted')).format(add)
        self.bot.additionalprefixdata[ctx.guild.id] = newprefix
        if newprefix == '': del self.bot.additionalprefixdata[ctx.guild.id]
        async with aiofiles.open('prefixes.txt','w') as file:
            await file.write(repr(self.bot.additionalprefixdata))
        await ctx.send(outmsg)
def setup(bot):
    bot.add_cog(Configuration(bot))