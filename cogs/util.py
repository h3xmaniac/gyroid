import discord
from discord.ext import commands



class Util():
    
    
    def __init__(self, bot):
        self.bot = bot
        
    @commands.command(hidden=True)
    async def ping(self,ctx):
        png = str(round(self.bot.latency * 1000,2))
        return await ctx.send("Pong! `"+png+'ms` :ping_pong:')
