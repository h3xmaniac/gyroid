import discord, aiofiles
from discord.ext import commands
from lib import uni
class Configuration:
    def __init__(self,bot):
        self.bot = bot
    #https://discord.gg/q2j8HtB
    @commands.command(help='info.version.help')
    async def version(self,ctx):
        await ctx.send((await uni.translate(ctx,'info.version.version')).format('0.2.1a'))
def setup(bot):
    bot.add_cog(Configuration(bot))