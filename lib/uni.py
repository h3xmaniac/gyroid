import i18n
from lib import db
i18n.load_path.append('locale')
i18n.set('fallback','en')
cache = {}
async def translate(ctx, transtr: str, over: str=None) -> str:
    if not over:
        global cache
        if ctx.author.id in cache:
            locale = cache[ctx.author.id]
        elif (await db.getprof(ctx.author.id,ctx,True))['locale']:
            locale = (await db.getprof(ctx.author.id,ctx,True))['locale']
        else:
            locale = 'en'
    else:
        locale = over
    cache[ctx.author.id] = locale
    i18n.set('locale', locale)
    return i18n.t(transtr)
async def clear_cache(ctx):
    global cache
    if ctx.author.id in cache:
        del cache[ctx.author.id]