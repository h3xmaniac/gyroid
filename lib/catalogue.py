import discord
from discord.ext import commands
from bs4 import BeautifulSoup as bs
import config
import aiohttp

class Catalog:
    def __init__(self, bot):
        self.bot = bot


    async def getData(self,kind,itm):
        url = f"http://moridb.com/items/{kind}/{itm}"
        #formatted_itm = itm.replace(' ', '-')
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as r:
                soup_data = await r.text()
                soup = bs(soup_data,"html.parser")
                return soup


    async def parse(self,soup):
        d = {}
        for i in range(0,len(soup.find_all('dt'))):
            d[soup.findAll('dt')[i].get_text().strip()] = soup.findAll('dd')[i].get_text().strip()

        #soup.findAll('dt')[3].get_text().strip()




#embed = discord.Embed(title='Alpine Wall')
#for i in range(0,len(list(d))):
#    embed.add_field(name=list(d.keys())[i], value=list(d.values())[i], inline=True)


def setup(bot):
    bot.add_cog(Catalog(bot))