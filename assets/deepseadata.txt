deepsea.seaweed | 200 | size.large | common.allday | X | X | X | X | X | X | X | - | - | - | X | X
deepsea.sea_grapes | 600 | size.small | common.allday | - | - | - | - | - | X | X | X | X | - | - | -
deepsea.sea_urchin | 800 | size.small | common.allday | - | - | - | - | X | X | X | X | X | - | - | -
deepsea.acorn_barnacle | 200 | size.very_small | common.allday | X | X | X | X | X | X | X | X | X | X | X | X
deepsea.oyster | 400 | size.small | common.allday | X | X | - | - | - | - | - | - | X | X | X | X
deepsea.turban_shell | 300 | size.small | 4 Pm - 9 Am | X | X | X | X | X | X | - | X | X | X | X | X
deepsea.abalone | 400 | size.medium | 4 Pm - 9 Am | X | X | X | X | X | X | X | X | X | X | X | X
deepsea.ear_shell | 300 | size.small | 4 Pm - 9 Am | X | X | X | X | X | X | X | X | X | X | X | X
deepsea.clam | 300 | size.small | common.allday | X | X | X | X | - | - | - | - | X | X | X | X
deepsea.pearl_oyster | 1,600 | size.small | common.allday | X | X | X | X | X | X | X | X | X | X | X | X
deepsea.scallop | 1,000 | size.medium | common.allday | X | X | X | X | X | X | X | X | X | X | X | X
deepsea.sea_anemone | 100 | size.medium | common.allday | X | X | X | X | X | X | X | X | X | X | X | X
deepsea.sea_star | 100 | size.small | common.allday | X | X | X | X | X | X | X | X | X | X | X | X
deepsea.sea_cucumber | 150 | size.medium | common.allday | X | X | X | X | - | - | - | - | - | - | X | X
deepsea.sea_slug | 200 | size.small | common.allday | X | X | X | X | X | X | X | X | X | X | X | X
deepsea.flatworm | 200 | size.small | 4 Pm - 9 Am | - | - | - | - | - | X | X | X | X | - | - | -
deepsea.mantis_shrimp | 1,250 | size.small | 4 Pm - 9 Am | X | X | X | X | X | X | X | X | X | X | X | X
deepsea.sweet_shrimp | 650 | size.small | 4 Pm - 9 Am | X | X | - | - | - | - | - | - | X | X | X | X
deepsea.tiger_prawn | 1,600 | size.small | 4 Pm - 9 Am | - | - | - | - | - | X | X | X | X | - | - | -
deepsea.spiny_lobster | 3,000 | size.large | 9 Pm - 4 Am | - | - | - | - | - | - | - | - | X | X | X | X
deepsea.lobster | 2,500 | size.large | common.allday | X | X | X | X | X | X | X | X | X | X | X | X
deepsea.snow_crab | 4,000 | size.large | common.allday | X | X | X | X | - | - | - | - | - | - | X | X
deepsea.horsehair_crab | 4,000 | size.medium | common.allday | X | X | X | X | - | - | - | - | - | - | X | X
deepsea.red_king_crab | 6,000 | size.large | common.allday | X | X | X | - | - | - | - | - | - | - | X | X
deepsea.spider_crab | 10,000 | size.huge | common.allday | - | - | X | X | - | - | - | - | - | - | - | -
deepsea.octopus | 1,000 | size.medium | common.allday | X | - | X | X | X | X | X | - | X | X | X | X
deepsea.spotted_garden_eel | 600 | size.small | common.allday | X | X | X | X | X | X | X | X | X | X | X | X
deepsea.chambered_nautilus | 900 | size.medium | common.allday | - | - | - | - | - | X | X | X | - | - | - | -
deepsea.horseshoe_crab | 1,500 | size.medium | 9 Pm - 4 Am | - | - | - | - | - | - | X | X | X | - | - | -
deepsea.giant_isopod | 9,000 | size.medium | common.allday | X | X | X | X | X | X | X | X | X | X | X | X