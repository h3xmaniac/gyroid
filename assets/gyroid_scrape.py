from bs4 import BeautifulSoup as BS
import aiohttp
import asyncio


async def get(*args, **kwargs):
    async with aiohttp.ClientSession() as client:
        response = await client.request('GET', *args, **kwargs)
        return await response.read()
async def download(url, name):
    content = await get(url)
    with open(f"img/{name}",'wb') as file:
        file.write(content)

async def function1():
    async with aiohttp.ClientSession() as session:
        async with session.get("https://animalcrossing.wikia.com/wiki/Deep-Sea_Creatures") as resp:
            data = await resp.read()
    parser = BS(data,"html.parser")
    bugtable = parser.find_all("table")[1].find_all("table")[0]
    rows = bugtable.find_all("tr")
    columns = [row.find_all("td") for row in rows if row.find_all("td")!=[]]
    a = []
    for x in range(0, len(columns)):
        b = []
        #link = columns[x][1].a["href"]
        #name = "deep-sea-creatures/" + columns[x][0].text.title().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','') + '.png'
        #await download(link,name)
        for y in range(0, len(columns[x])):
            if y ==0:
                name = columns[x][y].text.lower().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','')
                b.append(f"deepsea.{name}")
                continue
            if y ==1:
                continue
            if y ==3:
                shadowsize = columns[x][y].text.lower().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','')
                b.append(f"size.{shadowsize}")
                continue
            text =columns[x][y].text.title().strip("\n").strip(" ").strip("*")
            if text=="✓":
                b.append("X")
            elif text == "All Day":
                b.append("common.allday")
            else:
                b.append(text)
        a.append(b)
    #return
    c = [" | ".join(x) for x in a]
    d = "\n".join(c)
    with open("deepseadata.txt","w") as file:
        file.write(d)


async def function2():
    async with aiohttp.ClientSession() as session:
        async with session.get("https://animalcrossing.wikia.com/wiki/Bugs_(New_Leaf)") as resp:
            data = await resp.read()
    parser = BS(data,"html.parser")
    bugtable = parser.find_all("table")[1].find_all("table")[0]
    rows = bugtable.find_all("tr")
    columns = [row.find_all("td") for row in rows if row.find_all("td")!=[]]
    a = []
    for x in range(0, len(columns)):
        b = []
        #link = columns[x][1].a["href"]
        #name = "bugs/" + columns[x][0].text.title().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','') + '.png'
        #await download(link,name)
        for y in range(0, len(columns[x])):
            if y ==0:
                name = columns[x][y].text.lower().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','')
                b.append(f"bugs.{name}")
                continue
            if y ==1:
                continue
            if y ==3:
                location = columns[x][y].text.lower().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','')
                b.append(f"bugs.{location}")
                continue
            text =columns[x][y].text.title().strip("\n").strip(" ").strip("*")
            if text=="✓":
                b.append("X")
            elif text == "All Day":
                b.append("common.allday")
            else:
                b.append(text)
        a.append(b)
    #return
    c = [" | ".join(x) for x in a]
    d = "\n".join(c)
    with open("bugdata.txt","w") as file:
        file.write(d)

async def function3():
    async with aiohttp.ClientSession() as session:
        async with session.get("https://animalcrossing.wikia.com/wiki/Fish_(New_Leaf)") as resp:
            data = await resp.read()
    parser = BS(data,"html.parser")
    bugtable = parser.find_all("table")[1].find_all("table")[0]
    rows = bugtable.find_all("tr")
    columns = [row.find_all("td") for row in rows if row.find_all("td")!=[]]
    a = []
    for x in range(0, len(columns)):
        b = []
        #link = columns[x][1].a["href"]
        #name = "fish/" + columns[x][0].text.title().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','') + '.png'
        #await download(link,name)
        for y in range(0, len(columns[x])):
            if y ==0:
                name = columns[x][y].text.lower().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','')
                b.append(f"fish.{name}")
                continue
            if y ==1:
                continue
            if y ==3:
                location = columns[x][y].text.lower().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','')
                if columns[x][y].text.title().strip("\n").strip(" ").strip("*") == "Ocean (Rain or Snow)":
                    location = "ocean"
                b.append(f"fish.{location}")
                continue
            text =columns[x][y].text.title().strip("\n").strip(" ").strip("*")
            if text=="✓":
                b.append("X")
            elif text=="All Day":
                b.append("common.allday")
            else:
                b.append(text)
        a.append(b)
    #return
    c = [" | ".join(x) for x in a]
    d = "\n".join(c)
    with open("fishdata.txt","w") as file:
        file.write(d)

async def function4():
    async with aiohttp.ClientSession() as session:
        async with session.get("https://animalcrossing.wikia.com/wiki/Villager_list_(New_Leaf)") as resp:
            data = await resp.read()
    parser = BS(data,"html.parser")
    bugtable = parser.find_all("table")[1].find_all("table")[0]
    rows = bugtable.find_all("tr")
    columns = [row.find_all("td") for row in rows if row.find_all("td")!=[]]
    a = []
    for x in range(0, len(columns)):
        b = []
        #link = columns[x][1].a["href"]
        #name = "villagers/" + columns[x][0].text.title().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','') + '.png'
        #await download(link,name)
        for y in range(0, len(columns[x])):
            if y ==0:
                name = columns[x][y].text.lower().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','')
                b.append(f"villagers.{name}")
                continue    
            if y ==1:
                continue
            text =columns[x][y].text.title().strip("\n").strip(" ").strip("*")
            if y == 2:
                b.append("villagers.female" if "♀" in text else "villagers.male")
                personality = text[2:].lower().replace(' ','_').replace('(','').replace(')','')
                b.append(f"personalities.{personality}")
                continue
            if y == 3:
                species = columns[x][y].text.lower().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','')
                b.append(f"species.{species}")
                continue
            if y == 4:
                birthday  = columns[x][y].text.lower().strip("\n").strip(" ").strip("*").replace(' ','_').replace('(','').replace(')','').split("_")
                b.append(f"months.{birthday[0]} " + birthday[1])
                continue    
            b.append(text)
        a.append(b)
    #return
    c = [" | ".join(x) for x in a]
    d = "\n".join(c)
    with open("villagerdata.txt","w") as file:
        file.write(d)

async def function():
    await function1()
    await function2()
    await function3()
    await function4()

asyncio.get_event_loop().run_until_complete(function())
